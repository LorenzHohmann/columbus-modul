var settings = ['light', 'ventilation', 'electricity'];

$(function() {
	// database setting on start up
	for (var i = 0; i < settings.length; i++) {
		var setting = settings[i];
		$.getJSON('ajax/get_setting.php', { setting }, function(data) {
			$('#setting-' + data.setting).prop(
				'checked',
				data.value == 'true' ? true : false
			);
		});
	}

	// setting change function
	$('.change-setting').change(function() {
		var setting = $(this)
			.attr('id')
			.split('-')[1];
		var value = $(this).is(':checked');

		$.post('ajax/change_setting.php', { setting, value }, function(data) {
			if (data == 'success') {
				logger.log("Setting '" + setting + "' changed to " + value);
			} else {
				logger.log("Failed to change Setting '" + setting + "'");
			}
		});

		return false;
	});
});
