// logger
const logger = new Logger('console');
logger.log('Monitoring started ...');

// Load the Visualization API and the corechart package.
google.charts.load('current', { packages: ['corechart', 'gauge'] });

// build charts
google.charts.setOnLoadCallback(drawCharts);

function drawCharts() {
	// Temperatur innen
	new Chart(
		'Temperature',
		'Inside',
		'temperature_inside',
		'gauge',
		[['Label', 'Value'], ['in °C', 0]],
		{
			greenFrom: 20,
			greenTo: 40,
			yellowFrom: 40,
			yellowTo: 50,
			redFrom: 50,
			redTo: 200,
			minorTicks: 5,
			max: 200
		},
		1000
	).init();

	// Temperatur außen
	new Chart(
		'Temperature',
		'Outside',
		'temperature_outside',
		'gauge',
		[['Label', 'Value'], ['in °C', 0]],
		{
			greenFrom: 20,
			greenTo: 40,
			yellowFrom: 40,
			yellowTo: 50,
			redFrom: 50,
			redTo: 200,
			minorTicks: 5,
			max: 200
		},
		1000
	).init();

	// Luftqualität
	new Chart(
		'Air Quality',
		'',
		'air_issues',
		'donut',
		[
			['Inhaltsstoff', 'Prozent'],
			['Kohlenstoffdioxid', 0],
			['Rauch', 0],
			['LPG', 0],
			['Stickstoff', 78],
			['Sauerstoff', 21]
		],
		{
			pieHole: 0.2,
			backgroundColor: 'transparent',
			width: '230',
			height: '200',
			slices: {
				0: { color: 'blue' },
				1: { color: 'black' },
				2: { color: 'red' }
			},
			legend: 'none'
		},
		1000,
		['gasCo', 'gasSmoke', 'gasLpg']
	).init();

	// Luftdruck
	new Chart(
		'Air Pressure',
		'',
		'airpressure',
		'gauge',
		[['Label', 'Value'], ['in hPa', 0]],
		{
			greenFrom: 1000,
			greenTo: 1100,
			yellowFrom: 800,
			yellowTo: 1000,
			redFrom: 0,
			redTo: 800,
			minorTicks: 5,
			max: 1100
		},
		1000
	).init();

	// Motion
	// new Chart(
	// 	'Motion',
	// 	'',
	// 	'motion',
	// 	'gauge',
	// 	[['Label', 'Value'], ['in hPa', 0]],
	// 	{
	// 		greenFrom: 1000,
	// 		greenTo: 1100,
	// 		yellowFrom: 800,
	// 		yellowTo: 1000,
	// 		redFrom: 0,
	// 		redTo: 800,
	// 		minorTicks: 5,
	// 		max: 1100
	// 	},
	// 	1000
	// ).init();

	// Luftfeuchtigkeit
	new Chart(
		'Humidity',
		'',
		'humidity',
		'gauge',
		[['Label', 'Value'], ['in %', 0]],
		{
			greenFrom: 40,
			greenTo: 60,
			yellowFrom: 60,
			yellowTo: 75,
			redFrom: 75,
			redTo: 100,
			minorTicks: 5
		},
		1000
	).init();

	// Position
	new Chart(
		'Location',
		'',
		'location',
		'gauge',
		[['Label', 'Value'], ['in %', 0]],
		{
			greenFrom: 40,
			greenTo: 60,
			yellowFrom: 60,
			yellowTo: 75,
			redFrom: 75,
			redTo: 100,
			minorTicks: 5
		},
		1000
	).init();
}

logger.log('Application fallbacks initialized');
