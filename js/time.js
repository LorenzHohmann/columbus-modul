$(function() {
	setInterval(() => {
		const date = new Date();

		const day = date.getDate();
		const month = date.getMonth() + 1;
		const year = date.getFullYear();

		const hour = date.getHours();
		const minute = date.getMinutes();
		const second = date.getSeconds();

		$('#date').html(
			formatWithZero(day) +
				'.' +
				formatWithZero(month) +
				'.' +
				formatWithZero(year) +
				' ' +
				formatWithZero(hour) +
				':' +
				formatWithZero(minute) +
				':' +
				formatWithZero(second)
		);
	}, 1000);
});

function formatWithZero(number) {
	if (number < 10) {
		return '0' + number;
	}
	return number;
}
