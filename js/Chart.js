class Chart {
	constructor(
		title,
		subtitle,
		identifier,
		chartType,
		data,
		options,
		refreshTime = 1000,
		types = []
	) {
		this.title = title;
		this.subtitle = subtitle;
		this.identifier = identifier;
		this.chartType = chartType;
		this.data = google.visualization.arrayToDataTable(data);
		this.options = options;
		this.refreshTime = refreshTime;
		this.oldData = 0;
		this.types = types;

		this.chart = undefined;
		this.lastRefresh = 0;
	}

	init() {
		this.build();
		this.refresh(true);

		this.startRefreshInterval();
	}

	build() {
		if (this.subtitle == '') {
			this.subtitle = '&nbsp;';
		}

		const html =
			'<div class="chart" id="chart_' +
			this.identifier +
			'"></div>' +
			'<div class="no-data-found" style="display: none;"><h5 style="display: none;">404 - NO DATA FOUND!</h5><img src="img/red-cross.png"></div>' +
			'</div>' + 
			'<p class="refresh-interval">Refresh Interval: ' +
			this.refreshTime +
			'ms (last: <span class="last-refresh">0</span>ms ago)</p>';

		$('#' + this.identifier).html(html);

		$('.chart-grid')
			.hide()
			.fadeIn();

		switch (this.chartType) {
			case 'gauge':
				this.chart = new google.visualization.Gauge(
					document.getElementById('chart_' + this.identifier)
				);
				break;
			case 'donut':
				this.chart = new google.visualization.PieChart(
					document.getElementById('chart_' + this.identifier)
				);
				break;
			default:
				logger.log(
					"Chart '" + this.title + "': no valid chart type found!"
				);
				break;
		}
		if (this.chart != undefined) {
			this.chart.draw(this.data, this.options);
		}

		logger.log("Chart '" + this.title + "' initialized");
	}

	startRefreshInterval() {
		this.refreshInterval = setInterval(() => {
			this.refresh();
		}, this.refreshTime);
	}

	refresh() {
		const objElement = this;

		switch (this.chartType) {
			case 'gauge':
				$.post('ajax/get_data.php', { type: this.identifier }, function(
					res
				) {
					if (res == 'no data') {
						objElement.increaseLastRefresh();

						objElement.hideDataShowNoData();
					} else if (objElement.oldData != res) {
						objElement.hideNoDataShowData();

						objElement.data.setValue(0, 1, res);
						logger.log("'" + objElement.title + "': new data");
						objElement.oldData = res;

						objElement.increaseLastRefresh(true);
					} else {
						objElement.increaseLastRefresh();

						objElement.hideNoDataShowData();

						// logger.log('\'' + objElement.title + '\': same data');
					}
					objElement.chart.draw(objElement.data, objElement.options);
				}).fail(() => {
					objElement.increaseLastRefresh();

					logger.log(
						"'" + objElement.title + "': error while fetching data"
					);

					objElement.hideDataShowNoData();
				});

				break;
			case 'donut':
				$.post(
					'ajax/get_multi_data.php',
					{ types: this.types },
					function(res) {
						if (res == 'no data') {
							objElement.increaseLastRefresh();

							objElement.hideDataShowNoData();
						} else if (objElement.oldData != res) {
							objElement.hideNoDataShowData();

							const jsonRes = JSON.parse(res);

							for (let i = 0; i < jsonRes.length; i++) {
								const data = jsonRes[i];

								objElement.data.setValue(i, 1, data);
							}

							logger.log("'" + objElement.title + "': new data");
							objElement.oldData = res;

							objElement.increaseLastRefresh(true);
						} else {
							objElement.increaseLastRefresh();

							objElement.hideNoDataShowData();

							// logger.log('\'' + objElement.title + '\': same data');
						}
						objElement.chart.draw(
							objElement.data,
							objElement.options
						);
					}
				).fail(() => {
					objElement.increaseLastRefresh();

					logger.log(
						"'" + objElement.title + "': error while fetching data"
					);

					objElement.hideDataShowNoData();
				});

				break;
		}
	}

	getjQueryElement() {
		return $(document.getElementById('chart_' + this.identifier));
	}

	increaseLastRefresh(reset = false) {
		if (reset) {
			this.lastRefresh = 0;
		} else {
			this.lastRefresh += this.refreshTime;
		}
		this.getjQueryElement()
			.siblings('.refresh-interval')
			.children('.last-refresh')
			.html(this.lastRefresh);
	}

	hideDataShowNoData() {
		this.getjQueryElement()
			.hide()
			.siblings('.no-data-found')
			.show();
	}

	hideNoDataShowData() {
		this.getjQueryElement()
			.show()
			.siblings('.no-data-found')
			.hide();
	}
}
