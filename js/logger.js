class Logger {
	constructor(identifier) {
		this.console = $('#' + identifier);
		this.entries = [];
	}

	log(message) {
		const date = new Date();
		const currentTime =
			date.getFullYear() +
			'/' +
			date.getMonth() +
			'/' +
			date.getDate() +
			' ' +
			this.formatWithZero(date.getHours()) +
			':' +
			this.formatWithZero(date.getMinutes()) +
			':' +
			this.formatWithZero(date.getSeconds());

		this.entries.push('[' + currentTime + '] ' + message);

		const divHeight = this.console.parent('.col-content').height();
		const consoleNumber = Math.round(divHeight / 19);

		let fromI = this.entries.length - consoleNumber;

		this.console.html('');
		for (let i = fromI; i < this.entries.length; i++) {
			if (this.entries[i] != undefined) {
				this.console.append(this.entries[i] + '<br>');
			}
		}

		this.updateCounter();
	}

	updateCounter() {
		$('#console-entries').html(this.entries.length.toLocaleString());
	}

	formatWithZero(number) {
		if (number < 10) {
			return '0' + number;
		}
		return number;
	}
}
