<?php 
require_once("../lib/includer.php");

$types = $_REQUEST["types"];

if(empty($types)) {
	exit("no data");
}

$json_result = array();

foreach($types as $type) {
	$query = "SELECT value FROM module_values WHERE type = '" . $type . "' AND id = (SELECT MAX(id) FROM module_values WHERE type = '" . $type . "')";

	$result = $db->query($query);

	if(mysqli_num_rows($result)) {
		$row = mysqli_fetch_assoc($result);
		$json_result[] = $row["value"];
	}	
}

if(!empty($json_result)) {
	exit(json_encode($json_result));
}

exit("no data");
?>